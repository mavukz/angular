app.controller('ContactController', function($scope, $stateParams, TableService)
{
    var isEmployee;
    
    var tempContact = {};
    $scope.tempContact = $stateParams.contact;
    
    setYes = function()
    {        
        document.getElementById("rbYes").checked = true;
        document.getElementById("rbNo").checked = false;
        isEmployee = true;
    }
    
    setNo = function()
    {
        document.getElementById("rbNo").checked = true;
        document.getElementById("rbYes").checked = false;
        isEmployee = false;
    }
    
    var date = new Date().getFullYear();
    
    $scope.submitContact = function()
    {
        var contact = 
        {
            firstName: $scope.fName,
            lastName: $scope.lName,
            jobTitle: $scope.selectedJob,
            age:$scope.ageNew,
            nickname: 'yo yo yo',
            employee: isEmployee,
            email: $scope.emailNew
        };
        
        TableService.addData(contact);
        alert('saved');
    };
    
    var tempJson = angular.toJson($stateParams.contact);
    
    $scope.updateContact = function()
    {

        for(index in TableService.userDetails)
        {
            var element = angular.toJson(TableService.userDetails[index]);
            console.log(element);
            
            if(element == tempJson)
            {
                TableService.userDetails[index] = $scope.tempContact;
            }
            
        }
        
        alert('saved');
    };
    
    $scope.deleteContact = function(contact)
    {
        var objectValues = angular.toJson(contact);

        for(item of TableService.userDetails)
        {
            var elements = angular.toJson(item);

            if(elements == objectValues)
            {
                var arr = TableService.userDetails;
                var index = arr.indexOf(item);
                arr.splice(index, 1);
            }
        }
    };        
});
