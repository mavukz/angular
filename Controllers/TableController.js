app.controller('TableController', function($scope, $sce, $state, TableService)
{
    $scope.userDetailsController = TableService.getData();
    
    $scope.newState = function(contactObject)
    {
        $state.go('edit', {contact: contactObject, toEdit: true});
    }
    
    $scope.checkEmployee = function(isEmployee)
    {
        if(isEmployee){
           
           return $sce.trustAsHtml("<input type='checkbox' checked='true' onclick='this.checked=!this.checked'>");
           
       }else{
           
           return $sce.trustAsHtml("<input type='checkbox' onclick='this.checked=!this.checked'>");
           
       }
    }
        
})
