var app = angular.module('FirstAngularApp', ['ui.router'])
    .config(['$urlRouterProvider','$stateProvider', function($urlRouterProvider, $stateProvider, ContactController)
    {
        $urlRouterProvider.otherwise('/home');
        $stateProvider
            .state('home',
            {
                url: '/home',
                templateUrl: 'Views/home.html'
            })
            .state('search',
            {
                url:'/search',
                templateUrl:'Views/search.html'
            })
            .state('new',
            {
                url:'/new',
                templateUrl: 'Views/new.html',
            
            })
            .state('edit',
            {
                url: '/edit',
                templateUrl: 'Views/edit.html',
                params:
                {
                    contact: null,
                    toEdit: undefined
                }
            })
    }
    ]);
