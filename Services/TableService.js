app.service('TableService', function ()
{
    this.userDetails = [
        {
            firstName: 'Luntu',
            lastName: 'Mavukuza',
            jobTitle: 'Intern',
            age: 18,
            nickname:'Ma-VukZ',
            employee: true,
            email: 'luntu@cowboyaliens.com'            
        },
        {
            firstName: 'Keanu',
            lastName: 'Arendze',
            jobTitle: 'Intern',
            age: 18,
            nickname:'Kevin',
            employee: false,
            email: 'keanu@cowboyaliens.com'   
        },
        {
            firstName: 'Rossouw',
            lastName: 'Binedell',
            jobTitle: 'Intern',
            age: 18,
            nickname:'Ross',
            employee:true,
            email:'ross@cowboyaliens.com'   
        },
        {
            firstName: 'Amina',
            lastName: 'Latief',
            jobTitle: 'Intern',
            age: 18,
            nickname:'Aminaaa',
            employee: false,
            email: 'amina@cowboyaliens.com'   
        }
    ];
    
    this.getData = function()
    {
        return this.userDetails;
    }
    
    this.addData = function(contact)
    {
        this.userDetails.push(contact);
    }
    
})